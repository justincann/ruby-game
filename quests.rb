# ==============================================================================
# Quests Module - quest.rb
# ------------------------------------------------------------------------------
# In this module we inlcude every quests
# Side quests stay in
# ==============================================================================

module Quests

# ==============================================================================
# First Quest -
# ==============================================================================
  def Quests.first_quest()
    puts "\n\n\tJed Ayce leads you along a path until you reach a fork."
    puts "\t1. The left path leads into a brightly lit field. It appears empty."
    puts "\t2. The right path leads up a dark spiral hill. It is likely full of Gonowas."
    print "\n\tWhich path should we take #{$user_name}? "
    print "1 or 2? #{$prompt}"
    path_chosen = $stdin.gets.chomp

    if    path_chosen == "1" || path_chosen.downcase == "left"
            HelperMethods.clear()
            puts "\n\t Okay, so we will head into the field."
            Quests.field

    elsif path_chosen == "2" || path_chosen.downcase == "right"
            HelperMethods.clear()
            puts "\n\t You are quite brave. Let's head up the hill."
            Quests.hills

    else
          Messages.confused( "What? " )
    end

  end

#------------------------------------------------------------------------------
# Field

  def Quests.field
    puts "\n\t This is the field. It has rained recently, I won't be able to see her tracks"

  end

#------------------------------------------------------------------------------
# Hills

  def Quests.hills()
    puts "\n\t This hill is steep and rough. It will be easy to tell if she came this way."
    puts "\t Stay alert, I can already tell there are Gonowas here."
    puts "\t Normally they are peaceful, let's not upset them."

    print "\n\t Should we continue or go back? "
    print "#{$prompt}"
    path_chosen = $stdin.gets.chomp

    if    path_chosen == "1" || path_chosen.downcase == "continue"
            HelperMethods.clear()
            puts "\n\t Okay, let's continue up this hill."
            puts "\t Wait, what is that over there."
            Enemy.gonowa()

    elsif path_chosen == "2" || path_chosen.downcase == "go back"
            HelperMethods.clear()
            puts "\n\t Okay, we'll go back down the hill."
            # TODO: Go back sequence

    else
          Messages.confused( "What? " )
    end

  end

end
