# ==============================================================================
# Messages Module - messages.rb
# ------------------------------------------------------------------------------
# This module holds commonly stored modules
# ==============================================================================
module Messages
  def confused(message)
    puts message, 'Yeah, but what does that even mean?'
  end

  #---------------------------------------------------------------------------
  # Battle End
  def dead(why)
    puts why, 'Better luck next time.'
  end

  def success(enemy)
    puts "You have defeated the #{enemy}."
  end

  #---------------------------------------------------------------------------
  # Game Over
  def game_over
    line
    tabbed('GAME OVER')
  end
end
