# ==============================================================================
# Greeting
# ==============================================================================
class Scene
end

# Greeting scene
class Greeting < Scene
  attr_accessor :user_name, :gender, :prompt

  # def initialize(user_name, gender)
  #   @user_name = user_name
  #   @gender = gender
  #   @prompt = '> '
  # end

  def self.enter
    clear('quickly')
    intro
  end

  def self.intro
    tabbed('Well..')
    line
    questions
  end

  def self.questions
    tabbed("'What do people call you?' #{PROMPT}")
    user_name = $stdin.gets.chomp

    tabbed("'Welcome back to Geezia, #{user_name}.'")
    line
    tabbed("'Are you a man or a woman?' #{PROMPT}")
    gender = $stdin.gets.chomp
    the_gender(gender)
    tabbed("'Obviously! I'd be really worried if you forgot that.'")
    first_conversation
  end

  def self.first_conversation
    # tabbed("'Obviously! I'd be really worried if you forgot that.'")
    #
    # # background
    #
    line
    # clear
    tabbed("'You're the only person that I trust on this crazy planet. You'")
    tabbed('look so much like your sister. Pull up your hood, people may')
    tabbed('mistake you for her.') # print "#{prompt}"
    Clash.enter
  end

  def self.background
    tabbed('You have natural gift for battle yet you never trained.')
    tabbed('Your sister is missing on the planet Geizia and needs your help.')
    tabbed('She is a famous warrior and you share her talents. Learn to use')
    tabbed('them.')
  end
end
