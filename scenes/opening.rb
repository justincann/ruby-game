# ==============================================================================
# Opening Scene -
# ==============================================================================
class Scene
end

# Intro scene
class Opening < Scene
  def self.enter
    clear

    first_conversation
    first_story
    first_decision

    the_next = %w(Run Follow Stay)
    message = 'Enter a number to choose.'

    show_options(the_next, message)

    section = to_ordinal_number($stdin.gets.chomp)

    continue_on_to(the_next, section)
  end

  #-----------------------------------------------------------------------------
  # Conversations
  def self.first_conversation
    tabbed("'Hey'")
    short_pause
    tabbed("'Hey, are you awake?'")
    short_pause
    tabbed("'Get up, we need to get off this road.'")
    tabbed("'They will find us.'")
    short_pause
    tabbed("'GET UP!'")
    short_pause
    line
  end

  def self.second_conversation
    tabbed("'We will be safer here. But not for long.'")
    short_pause
    line
    tabbed("'What were you doing down there?'")

    second_decision
  end

  def self.third_conversation
    tabbed("'Wow, you're in a worst state that I realised.' 'How about this,")
    tabbed("tell me what you do remember. Starting with you name.'")
    Greeting.enter
  end

  #-----------------------------------------------------------------------------
  # Stories
  def self.first_story
    tabbed('Someone grabs your arm. They pull you up and rush you along the')
    tabbed('path. You are lying in the middle of a muddy road. The air is')
    tabbed("moist. It's too dark to see their face.")
    pause
  end

  def self.second_story
    clear('quickly')
    tabbed('You decide to continue to follow and they lead you farther along')
    tabbed("the path. \n")
    pause
    tabbed('They guide you into an open clearing. The lighting is better and')
    tabbed('now you can see their face.')
    line
    second_conversation
  end

  #-----------------------------------------------------------------------------
  # Decisions
  def self.first_decision
    line
    tabbed('They let go of your arm.')
    short_pause

    line
    tabbed('You realise that you have three options right now. You can run')
    tabbed('away, continue to follow, or stay where you are.')

    pause

    line
  end

  def self.second_decision
    options = ["'I don't remember how I got there.'",
               "'Who are you? Where are you taking me?'",
               "'How about you answer my questions instead.'"]
    message = 'Enter a number to choose.'

    show_options(options, message)

    $stdin.gets.chomp # does not get used
    third_conversation
  end

  # ============================================================================
  # == Outcomes

  #-----------------------------------------------------------------------------
  # First Outcomes
  def self.run
    clear
    tabbed('You decide to run away and take a different path. You still have')
    tabbed("no idea where to go it's too dark to see.\n")
    pause
    tabbed("Whoosh!!!\n")
    short_pause
    tabbed('A dark figure rushes past you. Another towards you. You have been')
    tabbed('stabbed! You fall backwards into the mud. And stay there.')
    pause
    game_over
  end

  def self.follow
    second_story
  end

  def self.stay
    clear
    tabbed('You decide to stay where you are. You watch them continue off into')
    tabbed("the darkness. If you only you could remember how you got there.\n")
    pause
    tabbed("SLASH!!!\n")
    short_pause
    tabbed('Everything goes dark, you fall down to your knees and draw your')
    tabbed('final breath.')
    pause
    game_over
  end
end
