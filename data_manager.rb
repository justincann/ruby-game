# ==============================================================================
# Data Manager Module Methods Module - data_manager.rb
# ------------------------------------------------------------------------------
# In this module we have functions for manipulating the json data
#
# ==============================================================================
module DataManager
  #-----------------------------------------------------------------------------
  def read_data(story, title)
    loaded_data = load_data(story, title)

    loaded_data.each_index do |i|
      data = loaded_data[i]

      function, message = extract_keys_and_values_from(data)

      execute_functions(function, message) # run a function
    end
  end

  def load_data(story, title)
    file = File.read('stories/data.json')

    data_hash = JSON.parse(file)
    data_hash[story][title]['content']
  end

  def extract_keys_and_values_from(data)
    function = data.keys.to_s
    message  = data.values.to_s
    message  = message.delete '[""]'
    function = function.delete '[""]'
    [function, message]
  end

  def execute_functions(function, message)
    function == 'tabbed' ? tabbed(message) : nil
    function == 'line' ? line : nil
    function == 'short_pause' ? short_pause : nil
    function == 'pause' ? pause : nil
  end
end
