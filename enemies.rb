# ==============================================================================
# Start - start.rb
# ------------------------------------------------------------------------------
# This is the startpoint
# ==============================================================================

#------------------------------------------------------------------------------

# Enemy Class
class Enemy
  attr_accessor :moods, :health, :strength
end

# Gonowa Class
class Gonowa < Enemy
  # Establish the health and strength of the Gonowa
  @health   = rand(2..5) * 15 # => random multiple of 15 between 30 - 75
  @strength = rand(2..5) * 10 # => random multiple of 10 between 20 - 50
  # @mood     = Enemy.moods[rand] # TODO: add other moods

  puts "\t Oh no, I see a gonowa"
  # puts "\t They appear to be #{@mood}."
  puts "\t They have #{@health} health."
  puts "\t They have #{@strength} strength."
end

# Gonowa.moods = %w(calm nervous aggresive)
