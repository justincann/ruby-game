# ==============================================================================
# Start - start.rb
# ------------------------------------------------------------------------------
# This is the startpoint
# ==============================================================================

#------------------------------------------------------------------------------
# External

require './helper_methods'
require './data_manager'
# # require './enemies'
require './shared'
# require './messages'
require './scenes'
require './scenes/opening'
require './scenes/greeting'
require './scenes/clash'
# require './quests'
require 'json'

#------------------------------------------------------------------------------
# Game Start
module Start
  def self.enter
    Clash.enter
  end
end
Start.enter
