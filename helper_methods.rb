# ==============================================================================
# Helper Methods Module - helper_methods.rb
# ------------------------------------------------------------------------------
# In this module we have reusable functions
#
# ==============================================================================
module HelperMethods
  #-----------------------------------------------------------------------------
  # Clear screen
  def clear(quickly = nil)
    do_nothing = ''
    quickly.nil? ? dots : do_nothing

    system 'cls'
    puts "\n\n\n\t"

    do_nothing = ''
    quickly.nil? ? dots : do_nothing

    system 'cls'
    puts "\n\n\n\t"
  end

  def dots
    system 'cls'
    print "\n\n\n\t ..."
    short_pause
  end

  #-----------------------------------------------------------------------------
  # Messaging

  def line
    puts "\n"
  end

  def tabbed(message)
    print "\n\t #{message}"
  end

  def show_story(message)
    print "\t #{message}"
  end

  def show_options(options, message = '')
    index = 0
    line

    options.each do |option|
      index += 1
      tabbed("#{index}. #{option}")
    end
    tabbed(message)
    print ' > '
  end

  def to_ordinal_number(number)
    number.to_i - 1
  end

  def continue_on_to(options, i)
    outcome = options[i].downcase
    send outcome
  end

  #-----------------------------------------------------------------------------
  # Pauses

  def pause(seconds = 5)
    sleep seconds
  end

  def short_pause
    sleep 2
  end

  #-----------------------------------------------------------------------------
  # Data handling
  # def the_gender(entered_gender)
  #   pronoun = entered_gender == 'male' ? 'he' : 'she'
  #   gender = entered_gender == 'male' ? 'man' : 'woman'
  #   result = [pronoun, gender]
  # end
end
